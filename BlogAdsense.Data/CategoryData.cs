﻿namespace BlogAdsense.Data
{
    using BlogAdsense.Data.Core;

    public class CategoryData : Core.EntityBaseData<Model.Category>
    {
        public CategoryData() : base(new DataContext())
        {

        }
    }
}
