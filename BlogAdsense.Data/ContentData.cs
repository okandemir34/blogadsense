﻿namespace BlogAdsense.Data
{
    using BlogAdsense.Data.Core;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ContentData : Core.EntityBaseData<Model.Content>
    {
        public ContentData() : base(new DataContext())
        {

        }

        public List<Model.Content> GetContentByPopulars()
        {
            var contents = _context.Set<Model.Content>().Where(a => a.IsActive && a.PublishDate <= DateTime.Now)
                .Take(6)
                .ToList();

            if (contents == null)
                return new List<Model.Content>();

            return contents;
        }

        public List<Model.Content> GetContentByEditorPick()
        {
            var contents = _context.Set<Model.Content>().Where(a => a.IsActive && a.PublishDate <= DateTime.Now)
                .OrderBy(x => Guid.NewGuid())
                .Take(5)
                .ToList();

            if (contents == null)
                return new List<Model.Content>();

            return contents;
        }
    }
}
