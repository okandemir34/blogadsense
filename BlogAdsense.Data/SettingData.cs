﻿namespace BlogAdsense.Data
{
    using BlogAdsense.Data.Core;

    public class SettingData : Core.EntityBaseData<Model.Setting>
    {
        public SettingData() : base(new DataContext())
        {

        }
    }
}
