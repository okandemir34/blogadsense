﻿namespace BlogAdsense.Data
{
    using BlogAdsense.Data.Core;

    public class UserData : Core.EntityBaseData<Model.User>
    {
        public UserData() : base(new DataContext())
        {

        }
    }
}
