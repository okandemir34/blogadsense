﻿using System;

namespace BlogAdsense.Model.Core
{
    public class IgnoreAttribute : Attribute
    {
        public string SomeProperty { get; set; }
    }
}
