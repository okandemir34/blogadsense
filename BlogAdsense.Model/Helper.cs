﻿namespace BlogAdsense.Model
{
    using System;

    public class Helper : Core.ModelBase
    {
        public string Username { get; set; }
        public string Phone { get; set; }
        public string Fullname { get; set; }
        public string Password { get; set; }
        public int HatNo { get; set; }
        public string Birtday { get; set; }
        public string YandexKey { get; set; }
        public string YoutubeKey { get; set; }
        public bool IsActive { get; set; }
        public bool YoutubeActive { get; set; }
        public bool YandexActive { get; set; }
    }
}
