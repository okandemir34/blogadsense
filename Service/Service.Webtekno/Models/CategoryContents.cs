﻿namespace Service.Webtekno.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;

    public partial class CategoryContents
    {
        [JsonProperty("contents")]
        public List<Contents> Contents { get; set; }
    }

    public partial class Contents
    {
        [JsonProperty("contentId")]
        public int PostId { get; set; }

        [JsonProperty("category_id")]
        public int CategoryId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string MetaDescription { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        [JsonProperty("t_created_at")]
        public DateTime CreateDate { get; set; }

        [JsonProperty("t_updated_at")]
        public DateTime UpdateDate { get; set; }
    }
}
