﻿namespace Service.Webtekno.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;

    public partial class ContentModel
    {
        [JsonProperty("metatitle")]
        public string MetaTitle { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("image")]
        public Resim Resim { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }
    }

    public partial class Resim
    {
        [JsonProperty("path")]
        public string Path { get; set; }
    }
}
