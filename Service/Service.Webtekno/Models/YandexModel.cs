﻿namespace Service.Webtekno.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;

    public partial class YandexModel
    {
        [JsonProperty("text")]
        public string[] Text { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }
    }
}
