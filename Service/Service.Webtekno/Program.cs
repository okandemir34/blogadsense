﻿namespace Service.Webtekno
{
    using Newtonsoft.Json;
    using System.Linq;
    using System.Net;
    using System.Text;
    using Service.Webtekno.Models;
    using BlogAdsense.Model;
    using System;
    using BlogAdsense.Data;
    using System.Threading;
    using HtmlAgilityPack;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    class Program
    {
        static void Main(string[] args)
        {
            //ChangeUserId();
            Category();
            Console.WriteLine("Hepsi Eklendi");
            Console.ReadLine();
        }
        static void ChangeUserId()
        {
            var _userData = new UserData();
            var _contentData = new ContentData();

            var contents = _contentData.GetAll();
            int count = 0;
            foreach (var item in contents)
            {
                item.UserId = _userData.GetRandom(1).FirstOrDefault().Id;
                var update = _contentData.Update(item);
                count++;
                Console.WriteLine(count);
            }
            Console.WriteLine("Bitti");
        }
        static void Category()
        {
            var _contentData = new ContentData();
            var _userData = new UserData();
            var categories = "Bilim,Donanim,Giyilebilir,internet,kampanya,kripto-para,mobil,mobil-uygulama,otomobil,oyun,sektorel,sinema,sosyal-medya,uzay,yapay-zeka,yasam,yazilim";
            //227,1,8,6,64,250,3,251,241,4,7,45,39,63,249,248,2
            string[] list = categories.Split(',');
            for (int i = 0; i < list.Count(); i++)
            {
                var adress = $"http://api.webtekno.com/solrContent?type=all&page=1&perpage=20&filter_query=categories&filter_value={list[i]}";
                var client = new WebClient();
                client.Encoding = Encoding.UTF8;
                var aciklama = client.DownloadString(adress);
                if (aciklama != null && !string.IsNullOrEmpty(aciklama))
                {
                    var CategoryContents = JsonConvert.DeserializeObject<CategoryContents>(aciklama);
                    foreach (var item in CategoryContents.Contents)
                    {
                        var listContent = _contentData.GetBy(x => x.PostId == item.PostId).FirstOrDefault();
                        if (listContent != null)
                            continue;

                        var tags = "";
                        if (item.Tags != null)
                        {
                            foreach (var text in item.Tags)
                            {
                                tags += text + ",";
                            }
                        }

                        var model = new Content()
                        {
                            Title = item.Title,
                            IsDeleted = false,
                            Slug = "",
                            CategoryId = i + 18,
                            CreateDate = item.CreateDate,
                            UpdateDate = item.UpdateDate,
                            PublishDate = DateTime.Now,
                            Description = "",
                            ImagePath = "",
                            IsActive = true,
                            Keywords = tags,
                            MetaDescription = item.MetaDescription,
                            MetaTitle = item.Title,
                            PostId = item.PostId,
                            UserId = _userData.GetRandom(1).FirstOrDefault().Id,
                        };

                        Content(model);
                    }

                }
            }
        }

        static void Content(Content model)
        {
            var _contentData = new ContentData();
            var adress = $"http://api.webtekno.com/news/detail/{model.PostId}";
            var client = new WebClient();
            client.Encoding = Encoding.GetEncoding(1254);
            client.Encoding = Encoding.UTF8;
            var aciklama = client.DownloadString(adress);
            if (aciklama != null && !string.IsNullOrEmpty(aciklama))
            {
                var content = JsonConvert.DeserializeObject<ContentModel>(aciklama);

                content.Content = content.Content.Replace("&ouml;", "ö")
                    .Replace("&ouml;", "ü")
                    .Replace("&ccedil;", "ç")
                    .Replace("&ccedil;", "ç")
                    .Replace("&nbsp;", " ")
                    .Replace("&Uuml;", "Ü")
                    .Replace("&uuml;", "ü")
                    .Replace("&Ouml;", "Ö")
                    .Replace("&lsquo;", "").Replace("&#39;", "")
                    .Replace("&ldquo;", "'")
                    .Replace("&rsquo;", "'");


                var imageList = new List<string>();

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(content.Content);
                var nodes = doc.DocumentNode.SelectNodes(@"//img[@src]");
                if (nodes != null)
                {
                    foreach (var img in nodes)
                    {
                        if (img.Attributes != null)
                        {
                            imageList.Add(img.Attributes[1].Value);
                        }
                    }
                }

                var guncelnews = content.Content;

                while (guncelnews.Contains("[NEWS"))
                {
                    var i = guncelnews.IndexOf("[NEWS");
                    var t = guncelnews.IndexOf("[/NEWS]");
                    var r = guncelnews.Substring(i, (t + 11 - i));
                    guncelnews = guncelnews.Replace(r, "");

                }
                while (guncelnews.Contains("[YOUTUBE"))
                {
                    var i = guncelnews.IndexOf("[YOUTUBE");
                    var t = guncelnews.IndexOf("[/YOUTUBE]");
                    var r = guncelnews.Substring(i, (t + 14 - i));
                    guncelnews = guncelnews.Replace(r, "");
                }
                while (guncelnews.Contains("[TWITTER"))
                {
                    var i = guncelnews.IndexOf("[TWITTER");
                    var t = guncelnews.IndexOf("[/TWITTER]");
                    var r = guncelnews.Substring(i, (t + 10 - i));
                    guncelnews = guncelnews.Replace(r, "");
                }

                string image = Regex.Replace(guncelnews, @"<img\s[^>]*>(?:\s*?</img>)?", "", RegexOptions.IgnoreCase);
                image = Regex.Replace(image, @"<video\s[^>]*>(?:\s*?</video>)?", "", RegexOptions.IgnoreCase);
                image = Regex.Replace(image, @"<a\s[^>]*>(?:\s*?</video>)?", "", RegexOptions.IgnoreCase);
                image = image.Replace("<p></p>", "");
                var insertmodel = new Content()
                {
                    CategoryId = model.CategoryId,
                    CreateDate = model.CreateDate,
                    Description = image,
                    ImagePath = "https://cdn.webtekno.com/media/cache/showcase_small_v2" + content.Resim.Path,
                    Slug = model.Slug,
                    IsActive = model.IsActive,
                    IsDeleted = model.IsDeleted,
                    Keywords = model.Keywords,
                    MetaDescription = content.Description,
                    MetaTitle = content.MetaTitle,
                    PostId = model.PostId,
                    PublishDate = model.PublishDate,
                    Title = content.Title,
                    UpdateDate = model.UpdateDate,
                    UserId = model.UserId,
                };

                insertmodel.Description = insertmodel.Description.Replace("<P>", "<p>").Replace("<H2>", "<h2>").Replace("<p>\r\n\r\n<p>", "<p>").Replace("&times;", "-");

                for (int i = 0; i < 5; i++)
                {
                    if (!string.IsNullOrEmpty(insertmodel.Description))
                    {
                        switch (i)
                        {
                            case 0:
                                if (!string.IsNullOrEmpty(insertmodel.Title))
                                {
                                    var ttitle = GetTranslate(insertmodel.Title);
                                    insertmodel.Title = ttitle;
                                }
                                break;
                            case 1:
                                if (!string.IsNullOrEmpty(insertmodel.Description))
                                {
                                    var tdesc = GetTranslate(insertmodel.Description);
                                    insertmodel.Description = tdesc;
                                }
                                break;
                            case 2:
                                if (!string.IsNullOrEmpty(insertmodel.MetaTitle))
                                {
                                    var tmetatitle = GetTranslate(insertmodel.MetaTitle);
                                    insertmodel.MetaTitle = tmetatitle;
                                }
                                break;
                            case 3:
                                if (!string.IsNullOrEmpty(insertmodel.MetaDescription))
                                {
                                    var tmetadesc = GetTranslate(insertmodel.MetaDescription);
                                    insertmodel.MetaDescription = tmetadesc;
                                }
                                break;
                            case 4:
                                if (!string.IsNullOrEmpty(insertmodel.Keywords))
                                {
                                    var tkeywords = GetTranslate(insertmodel.Keywords);
                                    insertmodel.Keywords = tkeywords;
                                }
                                break;
                            default:
                                Console.WriteLine("Çeviriler Tamam");
                                break;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(insertmodel.Description))
                {
                    if (!string.IsNullOrEmpty(insertmodel.MetaTitle))
                        insertmodel.Slug = Sluger.cevir(insertmodel.MetaTitle);

                    if (string.IsNullOrEmpty(insertmodel.Slug))
                        insertmodel.Slug = Sluger.cevir(insertmodel.Title);

                    insertmodel.Description = insertmodel.Description.Replace("<P>", "<p>").Replace("<H2>", "<h2>");
                    foreach (var item in imageList)
                    {
                        insertmodel.Description = insertmodel.Description + "<img src='https://www.webtekno.com" + item + "' alt='" + insertmodel.MetaTitle + "' />";
                    }

                    var insert = _contentData.Insert(insertmodel);
                    if (insert.IsSucceed)
                    {
                        Console.WriteLine(model.Title);
                        Console.WriteLine(insertmodel.Title + " Eklendi");
                    }
                }
            }
        }

        static string GetTranslate(string text)
        {
            var helper = new HelperData();
            var key = helper.GetRandom(1);
            var ykey = key[0].YandexKey;
            var trans = $"https://translate.yandex.net/api/v1.5/tr.json/translate?key={ykey}&text={text}&lang=en";
            var count = text.Length;
            if (count < 10000)
            {
                try
                {
                    var yclient = new WebClient();
                    yclient.Encoding = Encoding.GetEncoding(1254);
                    var yaciklama = yclient.DownloadString(trans);
                    var cevir = JsonConvert.DeserializeObject<YandexModel>(yaciklama);
                    return cevir.Text[0];
                }
                catch (Exception)
                {
                    return "";
                }

            }
            else
            {
                return "";
            }
        }
    }
}
