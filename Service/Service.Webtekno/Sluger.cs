﻿namespace Service.Webtekno
{
    using System.Text.RegularExpressions;

    public class Sluger
    {
        public static string cevir(string phrase)
        {
            string str = phrase.ToLower();
            str = str.Replace("ç", "c").Replace("ı", "i").Replace("ş", "s").Replace("ğ", "g").Replace("ö", "o").Replace("ü", "u").Replace("$", "s");
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            str = Regex.Replace(str, @"[\s-]+", " ").Trim();
            str = str.Substring(0, str.Length <= 100 ? str.Length : 100).Trim();
            str = Regex.Replace(str, @"\s", "-");

            return str;
        }
    }
}
