﻿namespace BlogAdsense.Snapyfox
{
    using System.Web.Mvc;
    using System.Web.Routing;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Categories",
                url: "all/categories",
                defaults: new { controller = "Category", action = "Index" }
            );

            routes.MapRoute(
                name: "CatDetail",
                url: "category/{slug}/{page}",
                defaults: new { controller = "Category", action = "Detail", page = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Content",
                url: "{slug}",
                defaults: new { controller = "Content", action = "Index" }
            );

            routes.MapRoute(
                name: "AmpContent",
                url: "amp/{slug}",
                defaults: new { controller = "Content", action = "Amp" }
            );

            routes.MapRoute(
                name: "SiteMap",
                url: "sitemap.xml",
                defaults: new { controller = "Home", action = "Sitemap" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
