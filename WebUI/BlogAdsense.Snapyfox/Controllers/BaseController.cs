﻿using BlogAdsense.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogAdsense.Snapyfox.Controllers
{
    public class BaseController : Controller
    {
        private SettingData _settingData;

        public BaseController()
        {
            _settingData = new SettingData();
        }

        public Model.Setting GetSiteSetting()
        {
            string siteId = ConfigurationManager.AppSettings["AdsBlog.SiteId"];
            //Site bilgisini alalim
            var setting = _settingData.GetByKey(Int32.Parse(siteId));
            return setting;
        }
    }
}