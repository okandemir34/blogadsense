﻿namespace BlogAdsense.Snapyfox.Controllers
{
    using BlogAdsense.Data;
    using BlogAdsense.Model;
    using BlogAdsense.Snapyfox.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class CategoryController : Controller
    {
        CategoryData _categoryData;
        ContentData _contentData;

        public CategoryController()
        {
            _categoryData = new CategoryData();
            _contentData = new ContentData();
        }

        public ActionResult Index()
        {
            var categories = _categoryData.GetAll();
            var model = new CategoryViewModel()
            {
                CategoryList = categories,
            };

            return View(model);
        }

        public ActionResult Detail(string slug, int page = 1)
        {
            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Error", "Home", new { e = "no-slug" });

            var category = _categoryData.GetBy(x => x.Slug == slug).FirstOrDefault();
            if (category == null)
                return RedirectToAction("Error", "Home", new { e = "category-not-found" });

            int _pageSize = 15;

            var contents = _contentData.GetByPage(x => x.CategoryId == category.Id,page, _pageSize, "Id",true);
            var count = _contentData.GetCount(x => x.CategoryId == category.Id);

            var model = new CategoryDetailViewModel()
            {
                Contents = contents,
                Category = category,
                TotalPageCount = (int)(count / _pageSize),
                CurrentPage = page
        };

            return View(model);
        }
    }
}