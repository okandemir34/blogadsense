﻿

namespace BlogAdsense.Snapyfox.Controllers
{
    using BlogAdsense.Data;
    using BlogAdsense.Snapyfox.Models;
    using BlogAdsense.Snapyfox.Filters;
    using BlogAdsense.Snapyfox.Helpers;
    using System.Web.Mvc;
    using BlogAdsense.Model;
    using System.Collections.Generic;
    using System.Linq;

    public class CmdController : BaseController
    {
        public Model.Setting Setting = new BaseController().GetSiteSetting();

        ContentData _contentData;
        SettingData _settingData;

        public CmdController()
        {
            _contentData = new ContentData();
            _settingData = new SettingData();
        }

        [Authentication]
        [HttpGet]
        public ActionResult Content(int page = 1)
        {
            var list = _contentData.GetByPage(x=>x.IsActive && !x.IsDeleted,page, 20, "Id", true);
            var count = _contentData.GetCount();

            var model = new ContentCmdListViewModel()
            {
                Count = count,
                CurrentPage = page,
                Contents = list
            };

            return View(model);
        }

        [Authentication]
        [HttpPost]
        public ActionResult Content(string username)
        {
            if (username.Length <= 3)
            {
                ViewBag.Message = "Arama Kelimesi en az 4 karakter olmlıdır";
                return View(new ContentCmdListViewModel()
                {
                    Count = 0,
                    CurrentPage = 1,
                    Contents = new List<Content>()
                });
            }

            var list = _contentData.GetBy(x => x.Title.Contains(username));
            ViewBag.Search = username;

            return View(new ContentCmdListViewModel()
            {
                Count = list.Count,
                CurrentPage = 1,
                Contents = list
            });
        }

        [Authentication]
        [HttpGet]
        public ActionResult ContentEdit(int id, int page = 1)
        {
            var p = _contentData.GetByKey(id);
            var model = new ContentEditViewModel()
            {
                Page = page,
                Content = p
            };

            return View(model);
        }

        [Authentication]
        [HttpPost, ValidateInput(false)]
        public ActionResult ContentEdit(Content content, int page)
        {
            var validationErrors = new List<string>();
            if (string.IsNullOrEmpty(content.Title))
                validationErrors.Add("Ad alanı zorunludur");
            if (string.IsNullOrEmpty(content.Description))
                validationErrors.Add("Snapchat kullanıcı alanı zorunludur");

            if (validationErrors.Count > 0)
            {
                ViewBag.Message = string.Join("<br />", validationErrors);
                return View(content);
            }

            var profileInDb = _contentData.GetByKey(content.Id);

            profileInDb.Title = content.Title;
            profileInDb.Description = content.Description;
            profileInDb.Keywords = content.Keywords;
            profileInDb.MetaDescription = content.MetaDescription;
            profileInDb.MetaTitle = content.MetaTitle;

            var result = _contentData.Update(profileInDb);

            ViewBag.Message = (result.IsSucceed)
                 ? "+Makale Güncellme İşlemi Tamamlandı"
                 : string.Join("<br />", "Makale Güncellerken Bir Hata Oldu", result.Message);

            return RedirectToAction("ContentEdit", new { id = profileInDb.Id, page = page });
        }

        [Authentication]
        [HttpGet]
        public ActionResult DeleteContent(int id, int page = 1, string returnAction = "Content")
        {
            var model = _contentData.GetByKey(id);
            model.IsActive = false;
            model.IsDeleted = true;
            var update = _contentData.Update(model);
            return RedirectToAction(returnAction, new { page = page });
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string UserName, string Password)
        {
            if (UserName.Equals("manager") && Password.Equals("123456"))
            {
                SessionHelper.IsLoggedIn = true;
                return RedirectToAction("Content", "Cmd");
            }

            return View();
        }

        [Authentication]
        [HttpGet]
        public ActionResult Settings()
        {
            var model = _settingData.GetBy(x => x.SiteId == Setting.Id).FirstOrDefault();
            return View(model);
        }

        [Authentication]
        [HttpPost, ValidateInput(false)]
        public ActionResult Settings(Setting model)
        {
            var validationErrors = new List<string>();
            if (string.IsNullOrEmpty(model.Ads300))
                model.Ads300 = "";
            if (string.IsNullOrEmpty(model.Ads600))
                model.Ads600 = "";
            if (string.IsNullOrEmpty(model.Analytics))
                validationErrors.Add("Analytics Kodu Ekle Bari Kanka");
            if (string.IsNullOrEmpty(model.Facebook))
                model.Facebook = "";
            if (string.IsNullOrEmpty(model.Instagram))
                model.Instagram = "";
            if (string.IsNullOrEmpty(model.LogoPath))
                model.LogoPath = "";
            if (string.IsNullOrEmpty(model.Pinterest))
                model.Pinterest = "";
            if (string.IsNullOrEmpty(model.SiteAdress))
                model.SiteAdress = "";
            if (string.IsNullOrEmpty(model.Sitename))
                model.Sitename = "";
            if (string.IsNullOrEmpty(model.Twitter))
                model.Twitter = "";
            if (string.IsNullOrEmpty(model.Youtube))
                model.Youtube = "";
            if (string.IsNullOrEmpty(model.AdsenseVerifiedCode))
                model.AdsenseVerifiedCode = "";
            

            if (validationErrors.Count > 0)
            {
                ViewBag.Message = string.Join("<br />", validationErrors);
                return View(model);
            }

            var dbInModel = _settingData.GetByKey(model.Id);

            dbInModel.Ads300 = model.Ads300;
            dbInModel.Ads600 = model.Ads600;
            dbInModel.Analytics = model.Analytics;
            dbInModel.Facebook = model.Facebook;
            dbInModel.Instagram = model.Instagram;
            dbInModel.LogoPath = model.LogoPath;
            dbInModel.Pinterest = model.Pinterest;
            dbInModel.SiteAdress = model.SiteAdress;
            dbInModel.Sitename = model.Sitename;
            dbInModel.Twitter = model.Twitter;
            dbInModel.Youtube = model.Youtube;
            dbInModel.AdsenseVerifiedCode = model.AdsenseVerifiedCode;
            dbInModel.IsSslDomian = model.IsSslDomian;

            var update = _settingData.Update(dbInModel);

            ViewBag.Message = (update.IsSucceed)
                 ? "+Ayarlar Güncellme İşlemi Tamamlandı"
                 : string.Join("<br />", "Ayarlar Güncellerken Bir Hata Oldu", update.Message);

            return View(model);
        }
    }
}