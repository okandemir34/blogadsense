﻿namespace BlogAdsense.Snapyfox.Controllers
{
    using BlogAdsense.Data;
    using BlogAdsense.Snapyfox.Models;
    using HtmlAgilityPack;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Mvc;

    public class ContentController : Controller
    {
        ContentData _contentData;

        public ContentController()
        {
            _contentData = new ContentData();
        }

        public ActionResult Index(string slug)
        {
            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Error", "Home", new { t = "no-slug" });

            var content = _contentData.GetBy(x => x.Slug == slug && x.IsActive).LastOrDefault();

            if (content == null)
                return View("Error", "Home", new { e = "not-found-content" });

            var related = _contentData.GetRandom(x => x.CategoryId == content.CategoryId, 3);

            var model = new ContentViewModel
            {
                Content = content,
                RelatedContent = related
            };

            return View(model);
        }

        public ActionResult Amp(string slug)
        {
            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Error", "Home", new { t = "no-slug" });

            var content = _contentData.GetBy(x=>x.Slug == slug && x.IsActive).LastOrDefault();

            if (content == null)
                return View("Error","Home", new { e = "not-found-content" });

            var related = _contentData.GetRandom(x => x.CategoryId == content.CategoryId, 4);

            var model = new ContentViewModel
            {
                Content = content,
                RelatedContent = related,
                ImgScrs = new List<string>()
            };

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(content.Description);
            var nodes = doc.DocumentNode.SelectNodes(@"//img[@src]");
            if (nodes != null)
            {
                foreach (var img in nodes)
                {
                    if (img.Attributes != null)
                    {
                        if (!string.IsNullOrEmpty(img.Attributes[0].Value)) { model.ImgScrs.Add(img.Attributes[0].Value); }
                        
                    }
                }
            }

            string description = content.Description;
            string image = Regex.Replace(description, @"<img\s[^>]*>(?:\s*?</img>)?", "", RegexOptions.IgnoreCase);
            image = Regex.Replace(image, @"<video\s[^>]*>(?:\s*?</video>)?", "", RegexOptions.IgnoreCase);
            ViewBag.Aciklama = image;

            return View(model);
        }
    }
}