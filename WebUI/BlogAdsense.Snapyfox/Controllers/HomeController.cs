﻿namespace BlogAdsense.Snapyfox.Controllers
{
    using BlogAdsense.Data;
    using BlogAdsense.Snapyfox.Helpers;
    using BlogAdsense.Snapyfox.Models;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Xml;

    public class HomeController : BaseController
    {
        Model.Setting Setting = new BaseController().GetSiteSetting();
        ContentData _contentData;

        public HomeController()
        {
            _contentData = new ContentData();
        }

        public ActionResult Index()
        {
            var banner = _contentData.GetBy(x => x.IsActive && !x.IsDeleted,"Id",true).Take(3).ToList();
            var trend = _contentData.GetBy(x => x.IsActive && !x.IsDeleted, "Id", true).Skip(3).Take(4).ToList();
            var last = _contentData.GetBy(x => x.IsActive && !x.IsDeleted, "Id", true).Skip(7).Take(15).ToList();

            var model = new HomeViewModel()
            {
                Banner = banner,
                TrendContents = trend,
                LastContents = last
            };

            return View(model);
        }

        public ActionResult Sitemap()
        {
            Response.Clear();
            Response.ContentType = "text/xml";
            XmlTextWriter xr = new XmlTextWriter(Response.OutputStream, Encoding.UTF8);
            xr.WriteStartDocument();
            xr.WriteStartElement("urlset");
            xr.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
            xr.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            xr.WriteAttributeString("xsi:schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd");

            var data = _contentData.GetBy(x => x.IsActive).OrderByDescending(x => x.PostId).ToList();

            foreach (var item in data)
            {
                xr.WriteStartElement("url");
                xr.WriteElementString("loc", Setting.SiteAdress + "/" + item.Slug);
                xr.WriteEndElement();
            }

            xr.WriteEndDocument();
            xr.Flush();
            xr.Close();
            Response.End();

            return View();
        }

        public ActionResult ClearCache()
        {
            CacheHelper.ClearCaches();
            return RedirectToAction("Index", "Home", new { s = "clear-cache" });
        }

        public JsonResult TestIp()
        {
            System.Web.HttpContext globalIP = System.Web.HttpContext.Current;
            string IPAdres = globalIP.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (!string.IsNullOrEmpty(IPAdres))
            {
                string[] adres = IPAdres.Split(',');
                if (adres.Length != 0) { return Json(adres[0], JsonRequestBehavior.AllowGet); }
            }

            var ip = globalIP.Request.ServerVariables["REMOTE_ADDR"];

            return Json(ip, JsonRequestBehavior.AllowGet);

        }
    }
}