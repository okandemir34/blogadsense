﻿namespace BlogAdsense.Snapyfox
{
    using BlogAdsense.Snapyfox.Helpers;
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!Request.IsLocal)
                DomainHelper.EnsureSslAndDomain(HttpContext.Current, true, CacheHelper.Settings.IsSslDomian);
        }
    }
}
