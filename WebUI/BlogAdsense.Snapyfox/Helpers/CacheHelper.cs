﻿namespace BlogAdsense.Snapyfox.Helpers
{
    using BlogAdsense.Data;
    using BlogAdsense.Model;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web;
    using System.Web.Caching;

    public static class CacheHelper
    {
        public static List<Category> Categories
        {
            get
            {
                if (HttpContext.Current.Cache["Categories"] == null)
                {
                    var category = new CategoryData().GetAll();

                    HttpContext.Current.Cache.Add(
                       "Categories"
                     , category
                     , null
                     , DateTime.Now.AddDays(1)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache["Categories"] as List<Category>;
            }
        }

        public static List<Content> EditorPick
        {
            get
            {
                if (HttpContext.Current.Cache["EditorPick"] == null)
                {
                    var category = new ContentData().GetRandom(x=>x.IsActive && x.PublishDate <= DateTime.Now,5);

                    HttpContext.Current.Cache.Add(
                       "EditorPick"
                     , category
                     , null
                     , DateTime.Now.AddDays(1)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache["EditorPick"] as List<Content>;
            }
        }

        public static Setting Settings
        {
            get
            {
                if (HttpContext.Current.Cache["Setting"] == null)
                {
                    string siteId = ConfigurationManager.AppSettings["AdsBlog.SiteId"];
                    var setting = new SettingData().GetByKey(Int32.Parse(siteId));

                    HttpContext.Current.Cache.Add(
                       "Setting"
                     , setting
                     , null
                     , DateTime.Now.AddDays(1)
                     , Cache.NoSlidingExpiration
                     , CacheItemPriority.AboveNormal
                     , null);
                }

                return HttpContext.Current.Cache["Setting"] as Setting;
            }
        }

        public static void ClearCaches()
        {
            HttpContext.Current.Cache.Remove("Setting");
        }
    }
}