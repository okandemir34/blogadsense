﻿namespace BlogAdsense.Snapyfox.Models
{
    using BlogAdsense.Model;
    using System.Collections.Generic;

    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
            CategoryList = new List<Category>();
        }

        public List<Category> CategoryList { get; set; }
    }
}