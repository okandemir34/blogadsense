﻿namespace BlogAdsense.Snapyfox.Models
{
    using BlogAdsense.Model;
    using System.Collections.Generic;

    public class ContentEditViewModel
    {
        public Content Content { get; set; }
        public int Page { get; set; }
    }
}