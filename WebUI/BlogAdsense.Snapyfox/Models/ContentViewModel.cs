﻿namespace BlogAdsense.Snapyfox.Models
{
    using BlogAdsense.Model;
    using System.Collections.Generic;

    public class ContentViewModel
    {
        public ContentViewModel()
        {
            RelatedContent = new List<Content>();
        }

        public List<Content> RelatedContent { get; set; }
        public Content Content { get; set; }
        public List<string> ImgScrs { get; set; }
    }
}