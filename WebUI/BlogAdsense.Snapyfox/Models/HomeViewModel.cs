﻿namespace BlogAdsense.Snapyfox.Models
{
    using BlogAdsense.Model;
    using System.Collections.Generic;

    public class HomeViewModel
    {
        public HomeViewModel()
        {
            TrendContents = new List<Content>();
            LastContents = new List<Content>();
            Banner = new List<Content>();
        }


        public List<Content> TrendContents { get; set; }
        public List<Content> LastContents { get; set; }
        public List<Content> Banner { get; set; }
    }
}